#ifndef IMAGE_ROTATE_
#define IMAGE_ROTATE_

#include <stdbool.h>
#include "image.h"


bool rotate90(struct image* img);

#endif
